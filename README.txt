
Overview
---------
This module allows for anonymous guests to keep persistent comment info 
via cookies and javascript.

Requirements
------------
* Drupal 5.0 or later

Authors
-------
Kenn Persinger (cainan) <lestat@iglou.com>
Matt Farina (mfer) <http://drupal.org/user/25701/contact>